# [1.2.0](https://gitlab.com/researchable/general/gems/jwt-authenticable/compare/v1.1.0...v1.2.0) (2023-10-11)


### Features

* allow skipping 2fa ([541de8a](https://gitlab.com/researchable/general/gems/jwt-authenticable/commit/541de8a7ac4cc2029c00b44f30dde2486b4dfb06))

# [1.1.0](https://gitlab.com/researchable/general/gems/jwt-authenticable/compare/v1.0.1...v1.1.0) (2023-06-30)


### Features

* enforce 2fa ([a65aed5](https://gitlab.com/researchable/general/gems/jwt-authenticable/commit/a65aed5e2ae0ad180239e51aa20dd9d2aa588e81))

## [1.0.1](https://gitlab.com/researchable/general/gems/jwt-authenticable/compare/v1.0.0...v1.0.1) (2023-06-14)


### Bug Fixes

* algorithm configuration parameter not being used ([b41ad24](https://gitlab.com/researchable/general/gems/jwt-authenticable/commit/b41ad24a5c9e58c3237160b531c8eab351c389eb))

# 1.0.0 (2023-04-14)


### Bug Fixes

* added a small info function for debugging ([f9428a8](https://gitlab.com/researchable/general/gems/jwt-authenticable/commit/f9428a8e77f9a52634d536327a78f771943fa226))


### Features

* **init:** initialize gem ([42c2c85](https://gitlab.com/researchable/general/gems/jwt-authenticable/commit/42c2c853ca705343c55b5f31edac394fab6b9237))
