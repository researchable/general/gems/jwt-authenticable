# frozen_string_literal: true

require_relative 'lib/jwt_authenticable/version'

Gem::Specification.new do |spec|
  spec.name = 'researchable_jwt-authenticable'
  spec.version = JwtAuthenticable::VERSION
  spec.authors = ['Researchable']
  spec.email = ['info@researchable.nl']

  spec.summary = "Researchable's gem to deal with JWT authentication"
  spec.description = <<-DESCRIPTION
    Researchable's JwtAuthenticable relies on Json Web Tokens to deal with authentication.
  DESCRIPTION

  spec.homepage = "https://gitlab.com/researchable/general/gems/jwt-authenticable/-/blob/v#{JwtAuthenticable::VERSION}/README.md"
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/researchable/general/gems/jwt-authenticable'
  spec.metadata['changelog_uri'] = "https://gitlab.com/researchable/general/gems/jwt-authenticable/-/blob/v#{JwtAuthenticable::VERSION}/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'dry-configurable', '~> 0.16'
  spec.add_dependency 'jwt', '~> 2.6'
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
