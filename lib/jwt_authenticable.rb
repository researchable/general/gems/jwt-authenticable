# frozen_string_literal: true

require 'dry-configurable'
require 'jwt'

# The module 'JwtAuth' provides jwt authentication for rails using the jwt gem
# @author Researchable
module JwtAuthenticable
  extend Dry::Configurable
  setting :algorithm
  # Note that for RSA algorithms this will actually be the public key
  setting :jwt_secret_key, default: nil

  # If set to true, a jwt will only be considered valid if 2fa has been enabled
  setting :enforce_2fa, default: false

  SUPPORTED_ALGOS = [JWT::Algos::Hmac, JWT::Algos::Rsa].freeze

  class Error < StandardError; end
end

require_relative 'jwt_authenticable/version'
require_relative 'jwt_authenticable/exceptions'
require_relative 'jwt_authenticable/responses'
require_relative 'jwt_authenticable/auth'
require_relative 'jwt_authenticable/info'
