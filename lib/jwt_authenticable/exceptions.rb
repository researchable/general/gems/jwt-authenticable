# frozen_string_literal: true

# Add here our custom exceptions
module JwtAuthenticable
  module Exceptions
    # Exception for invalid auth schemes. E.g. users trying to authenticate with basic auth
    class InvalidAuthScheme < StandardError
      def message
        'Invalid authentication scheme. Only Bearer is supported'
      end
    end

    # Exception when the includer is not invalid
    class InvalidIncluder < StandardError
      def message
        "The includer should export the 'request' method (i.e., it should be a rails controller)"
      end
    end

    # Exception for missing Authentication header
    class MissingAuth < StandardError
      def message
        'Authentication header must be present'
      end
    end

    # Exception for missing scopes on the JWT
    class MissingAuthScope < StandardError
      def initialize(scope)
        @scope = scope
        super(scope)
      end

      def message
        "Auth token must contain the #{@scope} scope"
      end
    end

    # Generic exception during the authorization process
    class AuthorizationError < StandardError
      def initialize(msg = nil)
        @msg = msg
        super(msg)
      end

      def message
        "Authorization error: #{@msg}"
      end
    end

    # Exception to raise when 2fa enforce is enabled but user has not enabled 2fa
    class TwoFANotEnabledError < StandardError
      def message
        '2FA must be enabled'
      end
    end
  end
end
