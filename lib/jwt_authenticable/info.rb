# frozen_string_literal: true

# This module gives some information about the gem configuration
module JwtAuthenticable
  # This module gives some information about the gem configuration
  module Info
    # This method prints the gem configuration
    def info
      return Rails.logger.info JwtAuthenticable.config if defined?(Rails)

      puts JwtAuthenticable.config # rubocop:disable Rails/Output
    end
  end
end
