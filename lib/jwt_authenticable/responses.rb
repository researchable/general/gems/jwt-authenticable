# frozen_string_literal: true

module JwtAuthenticable
  # Provides a suite of methods to return standardized JSON Http responses
  module Responses
    # renders a resource (eg json file) or rendes an error of there are errors in the resource
    def render_resource(resource)
      if resource.errors.empty?
        render json: resource
      else
        validation_error(resource.errors)
      end
    end

    # Method to render validation errors in a consistent way
    # @param resource_errors the errors to render
    def validation_error(resource_errors)
      render json: {
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: resource_errors,
            code: '100'
          }
        ]
      }, status: :bad_request
    end

    # Method to render access denied errors in a consistent way
    # @param resource_errors the errors to render
    def access_denied(resource_errors)
      render json: {
        errors: [
          {
            status: '403',
            title: 'Access Denied',
            detail: resource_errors,
            code: '100'
          }
        ]
      }, status: :forbidden
    end

    # Method to render not found errors in a consistent way
    # @param resource_errors the errors to render
    def not_found(resource_errors)
      render json: {
        errors: [
          {
            status: '404',
            title: 'Not Found',
            detail: resource_errors,
            code: '100'
          }
        ]
      }, status: :not_found
    end

    # Method to render created status in a consistent way
    def created(instance = nil)
      render json: {
        result: [
          {
            status: '201',
            title: 'created',
            detail: 'resource created',
            code: '100',
            instance: instance
          }
        ]
      }, status: :created
    end

    # Method to render success status in a consistent way
    def render_success_resource(resource)
      render json: {
        result: [
          {
            status: '200',
            title: 'Success',
            detail: resource,
            code: '100'
          }
        ]
      }, status: :ok
    end

    # Method to render accepted status in a consistent way
    def accepted
      render json: {
        result: [
          {
            status: '202',
            title: 'Accepted',
            detail: 'Your request will be processed',
            code: '100'
          }
        ]
      }, status: :accepted
    end

    # Method to render destroyed status in a consistent way
    def destroyed
      render json: {
        result: [
          {
            status: '200',
            title: 'destroyed',
            detail: 'resource destroyed',
            code: '100'
          }
        ]
      }, status: :ok
    end

    # Method to render unprocessable entity errors in a consistent way
    # @param resource_errors the errors to render
    def unprocessable_entity(resource_errors)
      render json: {
        errors: [
          {
            status: '422',
            title: 'unprocessable',
            detail: resource_errors,
            code: '100'
          }
        ]
      }, status: :unprocessable_entity
    end

    # Method to render no content in a consistent way
    def no_content
      render json: {
        result: [
          {
            status: '204',
            title: 'no content',
            detail: 'no content provided',
            code: '100'
          }
        ]
      }, status:  :no_content
    end

    def unauthorized(detail)
      render json: {
        result: [
          {
            status: '401',
            title: 'unauthorized',
            detail: detail || 'invalid or missing credentials',
            code: '100'
          }
        ]
      }, status: :unauthorized
    end

    def render_page(paginated_query, _config = {})
      render json: {
        data: paginated_query,
        page: {
          current_page: paginated_query.current_page,
          next_page: paginated_query.next_page,
          total_count: paginated_query.total_count,
          total_pages: paginated_query.total_pages,
          has_next: !paginated_query.last_page? && paginated_query.size.positive?
        }
      }
    end

    def payload_too_large(_details)
      render json: {
        result: [
          {
            status: '413',
            title: 'payload too large',
            details: nil,
            code: '100'
          }
        ]
      }, status: :payload_too_large
    end
  end
end
