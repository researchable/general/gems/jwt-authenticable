# frozen_string_literal: true

RSpec.describe JwtAuthenticable do
  let(:test_class) do
    Class.new do
      include JwtAuthenticable::Auth
    end
  end

  let(:subject) { test_class.new }

  it 'adds the right methods to a class including the file' do
    expect(subject).not_to be nil
    expect(subject.methods).to include :authenticate_user!
    expect(subject.methods).to include :validate_jwt_token!
    expect(subject.methods).to include :authorization_token!
  end

  describe 'validate_jwt_token!' do
    let(:token) do
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' \
        'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.' \
        'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
    end

    let(:valid_key) { 'your-256-bit-secret' }
    let(:valid_response) { { 'iat' => 1_516_239_022, 'name' => 'John Doe', 'sub' => '1234567890' } }
    it 'should raise if there is no verification key available' do
      expect { subject.validate_jwt_token!(token: token) }.to raise_error(JWT::DecodeError)
    end

    it 'should raise if the key is wrong' do
      JwtAuthenticable.config.jwt_secret_key = 'invalid_key'
      expect { subject.validate_jwt_token!(token: token) }.to raise_error(JWT::VerificationError)
    end

    it 'should not raise if the algorithm is wrong (fall back to default)' do
      JwtAuthenticable.config.jwt_secret_key = valid_key
      JwtAuthenticable.config.algorithm = 'lkfsalkdsajfls'
      expect(subject.validate_jwt_token!(token: token)).to eq valid_response
    end

    it 'should return the result if the jwt is correct' do
      JwtAuthenticable.config.jwt_secret_key = valid_key
      expect(subject.validate_jwt_token!(token: token)).to eq valid_response
    end

    context 'when enforce_2fa is true' do
      # The payload for this token is:
      # {
      #   "sub": "1234567890",
      #   "name": "John Doe",
      #   "iat": 1516239022,
      #   "2fa": true
      # }
      let(:valid_token) do
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' \
          'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCIyZmEiOnRydWV9.' \
          'P2gPLw3FFdiLAhkDxuoJT3EvSHSWrRoZYcYwYk2O-FM'
      end
      # The payload for this token is:
      # {
      #   "sub": "1234567890",
      #   "name": "John Doe",
      #   "iat": 1516239022
      # }
      let(:invalid_token) do
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' \
          'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.' \
          'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
      end
      let(:valid_response) { { 'iat' => 1_516_239_022, 'name' => 'John Doe', 'sub' => '1234567890', '2fa' => true } }
      it 'should not raise if 2fa payload attribute is true' do
        JwtAuthenticable.config.enforce_2fa = true
        expect(subject.validate_jwt_token!(token: valid_token)).to eq valid_response
      end
      it 'should raise if 2fa payload attribute is false' do
        JwtAuthenticable.config.enforce_2fa = true
        expect do
          subject.validate_jwt_token!(token: invalid_token)
        end.to raise_error(JwtAuthenticable::Exceptions::TwoFANotEnabledError)
      end

      context 'when skip_2fa is true' do
        it 'should not raise if 2fa payload attribute is false' do
          JwtAuthenticable.config.enforce_2fa = true
          expect { subject.validate_jwt_token!(token: invalid_token, skip_2fa: true) }.not_to raise_error
        end
      end
    end

    context 'using RS256 algorithm' do
      before(:each) do
        JwtAuthenticable.config.algorithm = 'RS256'
        JwtAuthenticable.config.enforce_2fa = false
      end

      let(:token) do
        'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.' \
          'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.' \
          'CtTyxPVtdkU_f7Ra9dQP_Gai1QBVZNC__ALKzIaCm-rHt9HBnNtCCDED6V8B979-UZx3onridlaQo3iTucPJetbAtkWX03Ptfq0y4L7J80' \
          'VYbT60F8GpE3Og0wyZUSROJybaY1o2iWx_M2FwGSFhVKon8xOYrQqBuDMwS9cD9ZMKKvuJ7p3OyQqOuW10-5EBTtyFV-sMP_9Bcj6Xvnce' \
          '0PVQf9AaVNjuhQHHnz_O3LzSqRJz9e1lfkFanbWrXKsV8LV1KISdg_U2S7af-ztRwxDGOV_GvOxPqcUHSpTYDJXtUv6FI3aeNhK2WhmAPO' \
          'mUdmm7TE93KZnp0Q29RKYwpw'
      end

      let(:valid_key) do
        public_key = <<~KEY
          -----BEGIN PUBLIC KEY-----
          MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp4ark79c9x5/Hfu3Vvvd
          FV+TLoevT/Zpd7QaxFmJE8NgD5qWUuxCdWSru5cc6zqPD9JcVB8yJSFcBooxxsW1
          iD3xSLpJHRwwcAjI5SLnHyVPMs7p1TM12JbSv8UPKf0fqOcksyeJ7P2IWmvRBkmp
          dNlLwUZKZtq/4UvVm6mjLcOxcd7YRCEKGQJSPqjVU5VGo083aT149c5fou32izfw
          c1bWyRwFUO2WsjFzsgUJW0jelkFKitd/xYIVCsjSYwVhHya7wAVkiZRzL3mGAptc
          UDL2AcrOJqmBo9yXcpfZtjG0lFQdu0/hgDcGUpisXPseMblWHB7cmZLhQaKeR++K
          OwIDAQAB
          -----END PUBLIC KEY-----
        KEY
        OpenSSL::PKey::RSA.new(public_key)
      end

      let(:invalid_key) do
        public_key = <<~KEY
          -----BEGIN PUBLIC KEY-----
          MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp4ark79c9x5/Hfu3Vvvd
          FV+TLoevT/Zpd7QaxFmJE8NgD5qWUuxCdWSru5cc6zqPD9JcVB8yJSFcBooxxsW1
          iD3xSLpJHRwwcAjI5SLnHyVPMs7p1TM12JbSv8UPKf0fqOcksyeJ7P2IWmvRBkmp
          dNlLwUZKZtq/4UvVm6mjLcOxcd7YRCEKGQJSPqjVU5VGo083aT149c5fou32izfw
          c1bWyRwFUO2WsjFzsgUJW0jelkFKitd/xYIVCsjSYwVhHya7wAVkiZRzL3mGAptc
          UDL2AcrOJqmBo9yXcpfZtjG0lFQdu0/hgDcGUpisXPseMblWHB7cmZLhQaKeR++K
          OwIDAQAb
          -----END PUBLIC KEY-----
        KEY
        OpenSSL::PKey::RSA.new(public_key)
      end

      let(:valid_response) { { 'iat' => 1_516_239_022, 'name' => 'John Doe', 'admin' => true, 'sub' => '1234567890' } }

      it 'should validate the key with the public key' do
        JwtAuthenticable.config.jwt_secret_key = valid_key
        expect(subject.validate_jwt_token!(token: token)).to eq valid_response
      end

      it 'should raise if the public key does not match the secret key' do
        JwtAuthenticable.config.jwt_secret_key = invalid_key
        expect { subject.validate_jwt_token!(token: token) }.to raise_error(JWT::VerificationError)
      end
    end
  end

  describe 'authorization_token!' do
    it 'should raise an exception when the class is not a controller / does not have a request' do
      expect { subject.authenticate_user! }.to raise_error(JwtAuthenticable::Exceptions::InvalidIncluder)
    end

    it 'should raise an exception no auth header is present' do
      mock_request = double('request')
      allow(mock_request).to receive(:headers).and_return({})
      allow(mock_request).to receive(:cookies).and_return({})
      allow(subject).to receive(:request).and_return(mock_request)
      expect(subject).to receive(:unauthorized).with('Authentication header must be present')

      subject.authenticate_user!
    end

    it 'should raise an InvalidAuthScheme if token is not a bearer token' do
      mock_request = double('request')
      allow(mock_request).to receive(:headers).and_return({ 'Authorization' => 'NotBearer abc' })
      allow(mock_request).to receive(:cookies).and_return({})
      allow(subject).to receive(:request).and_return(mock_request)

      expect(subject).to receive(:unauthorized).with('Invalid authentication scheme. Only Bearer is supported')

      subject.authenticate_user!
    end
  end
end
