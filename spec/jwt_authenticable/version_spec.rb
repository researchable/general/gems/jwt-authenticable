# frozen_string_literal: true

RSpec.describe JwtAuthenticable do
  it 'has a version number' do
    expect(JwtAuthenticable::VERSION).not_to be nil
  end
end
